"""Calories_calculator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django_registration.backends.one_step.views import RegistrationView
from django.contrib.auth.views import LogoutView, LoginView
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static

from core.forms import CustomUserForm
from core.views import Index, Profile, CategoryView, ChoiceCategoryView, ProductsView, ProductOffer, Statistics, Settings
# from core.forms import CustomUserForm
# from core.views import Index, Profile, CategoryView, ProductsView, ChoiceCategoryView, Statistics, Settings, \
#     ProductOffer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Index.as_view()),
    path('accounts/register/', RegistrationView.as_view(success_url='/', form_class=CustomUserForm),
         name='django_registration_register'),
    path('accounts/', include('django_registration.backends.activation.urls')),
    path('accounts/registration/', LoginView.as_view(), name='registration'),
    path('accounts/profile/', Profile.as_view(), name='profile'),
    path('accounts/profile/<int:pk>', Profile.as_view(), name='user_profile'),
    path('accounts/logout/', LogoutView.as_view(), name='logout'),
    path('change-password/',
         auth_views.PasswordChangeView.as_view(template_name='registration/password_change_form.html'),
         name='password_change'),
    path('change-password/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('category/<int:pk>', CategoryView.as_view(), name='category'),
    path('product/<int:pk>', ProductsView.as_view(), name='product'),
    path('product/offer', ProductOffer.as_view(), name='product_offer'),
    path('choice_category/', ChoiceCategoryView.as_view(), name='choice_category'),
    path('statistics/<int:pk>', Statistics.as_view(), name='statistics'),
    path('settings/', Settings.as_view(), name='settings'),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      path('__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
