from __future__ import absolute_import, unicode_literals

from .models import UserReward, UserProduct, CustomUser, Reward

from Calories_calculator import celery_app
from datetime import datetime, timedelta


@celery_app.task
def rewards_for_the_week():
    now = datetime.now() - timedelta(weeks=1)
    user_calories = {}
    for user in UserProduct.objects.filter(created_at__gte=now).select_related(
            'user_id').prefetch_related('user_id'):
        if str(user.user_id) in user_calories.keys():
            user_calories[f'{user.user_id}'] += user.us_pr_calories
        else:
            user_calories[f'{user.user_id}'] = user.us_pr_calories
    if user_calories:
        thin = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[0][0]
        thin_reward = UserReward()
        thin_reward.user_id = CustomUser.objects.get(username=thin)
        thin_reward.reward_id = Reward.objects.create(title=1)
        thin_reward.save()
        fat = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[-1][0]
        fat_reward = UserReward()
        fat_reward.user_id = CustomUser.objects.get(username=fat)
        fat_reward.reward_id = Reward.objects.create(title=4)
        fat_reward.save()
        return thin_reward, fat_reward
    return None, None


@celery_app.task
def rewards_for_the_month():
    now = datetime.now() - timedelta(days=30)
    user_calories = {}
    for user in UserProduct.objects.filter(created_at__gte=now).select_related(
            'user_id').prefetch_related('user_id'):
        if str(user.user_id) in user_calories.keys():
            user_calories[f'{user.user_id}'] += user.us_pr_calories
        else:
            user_calories[f'{user.user_id}'] = user.us_pr_calories
    if user_calories:
        thin = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[0][0]
        thin_reward = UserReward()
        thin_reward.user_id = CustomUser.objects.get(username=thin)
        thin_reward.reward_id = Reward.objects.create(title=2)
        thin_reward.save()
        fat = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[-1][0]
        fat_reward = UserReward()
        fat_reward.user_id = CustomUser.objects.get(username=fat)
        fat_reward.reward_id = Reward.objects.create(title=5)
        fat_reward.save()
        return thin_reward, fat_reward
    return None, None


@celery_app.task
def rewards_for_the_year():
    now = datetime.now() - timedelta(days=365)
    user_calories = {}
    for user in UserProduct.objects.filter(created_at__gte=now).select_related(
            'user_id').prefetch_related('user_id'):
        if str(user.user_id) in user_calories.keys():
            user_calories[f'{user.user_id}'] += user.us_pr_calories
        else:
            user_calories[f'{user.user_id}'] = user.us_pr_calories
    if user_calories:
        thin = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[0][0]
        print(thin)
        thin_reward = UserReward()
        thin_reward.user_id = CustomUser.objects.get(username=thin)
        thin_reward.reward_id = Reward.objects.create(title=3)
        thin_reward.save()
        fat = sorted(user_calories.items(), key=lambda item: (item[1], item[0]))[-1][0]
        print(fat)
        fat_reward = UserReward()
        fat_reward.user_id = CustomUser.objects.get(username=fat)
        fat_reward.reward_id = Reward.objects.create(title=6)
        fat_reward.save()
        return thin_reward, fat_reward
    return None, None
