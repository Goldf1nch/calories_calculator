from django.views.generic.base import TemplateView
from django.views.generic import CreateView
from django.shortcuts import redirect
from django.db.models import Sum, Q
from django.http import Http404
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Product, Category, CustomUser, UserProduct, UserReward
from .forms import SettingsForm, ProductOfferForm
# from .tasks import rewards_for_the_week, rewards_for_the_year, rewards_for_the_month

from datetime import datetime, timedelta


class Index(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['fats'] = CustomUser.objects.filter(user_calories__gt=0).order_by('-user_calories')[:10]
        context['thins'] = CustomUser.objects.filter(user_calories__gt=0).order_by('user_calories')[:10]
        # rewards_for_the_week()
        couple_for_the_week = UserReward.objects.filter(
            Q(created_at__gte=(datetime.now() - timedelta(weeks=1)), reward_id__title=1) |
            Q(created_at__gte=(datetime.now() - timedelta(weeks=1)), reward_id__title=4)
        ).select_related('reward_id', 'user_id').prefetch_related('reward_id', 'user_id')
        if couple_for_the_week:
            context['thin_for_the_week'] = couple_for_the_week[0].user_id
            context['fat_for_the_week'] = couple_for_the_week[1].user_id
        # rewards_for_the_month()
        couple_for_the_month = UserReward.objects.filter(
            Q(created_at__gte=(datetime.now() - timedelta(days=30)), reward_id__title=2) |
            Q(created_at__gte=(datetime.now() - timedelta(days=30)), reward_id__title=5)
        ).select_related('reward_id', 'user_id').prefetch_related('reward_id', 'user_id')
        if couple_for_the_month:
            context['thin_for_the_month'] = couple_for_the_month[0].user_id
            context['fat_for_the_month'] = couple_for_the_month[1].user_id
        # rewards_for_the_year()
        couple_for_the_year = UserReward.objects.filter(
            Q(created_at__gte=(datetime.now() - timedelta(days=365)), reward_id__title=3) |
            Q(created_at__gte=(datetime.now() - timedelta(days=365)), reward_id__title=6)
        ).select_related('reward_id', 'user_id').prefetch_related('reward_id', 'user_id')
        if couple_for_the_year:
            context['thin_for_the_year'] = couple_for_the_year[0].user_id
            context['fat_for_the_year'] = couple_for_the_year[1].user_id
        return context


class ChoiceCategoryView(LoginRequiredMixin, TemplateView):
    template_name = "product/choice_category.html"

    def get_context_data(self, **kwargs):
        context = super(ChoiceCategoryView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all().order_by('name')
        return context


class CategoryView(LoginRequiredMixin, TemplateView):
    template_name = "product/category.html"

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        id_ = kwargs['pk']  # Взять ID категории продукта из URL адреса
        context['products'] = Product.objects.filter(category__id=id_, is_deleted=False).order_by('name')
        return context


class ProductsView(LoginRequiredMixin, TemplateView):
    template_name = "product/products.html"

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        id_ = kwargs['pk']
        context['product'] = Product.objects.get(id=id_)
        return context

    def post(self, request, *args, **kwargs):
        user_product = UserProduct()  # создание экземпляра UserProduct
        user_id = self.request.user.id  # получение ИД текущего пользователя
        product = Product.objects.get(id=kwargs['pk'])  # получение объекта необходимого продукта
        custom_user = CustomUser.objects.get(id=user_id)  # получение объекта необходимого пользователя
        user_product.prod_id = product  # запись ИД продукта в UserProduct
        user_product.user_id = custom_user  # запись ИД пользователя в UserProduct
        weight = request.POST["weight"]  # получение веса продукта от пользователя
        user_product.us_pr_calories = (float(product.calories) / 100) * float(
            weight)  # расчет калорийности продукта и добавление в UserProduct
        custom_user.user_calories += user_product.us_pr_calories  # добавление калорий пользователю
        custom_user.save()
        user_product.weight = weight
        user_product.save()
        return redirect('/')


class ProductOffer(LoginRequiredMixin, CreateView):
    template_name = "product/product_offer.html"
    model = Product
    form_class = ProductOfferForm

    def form_valid(self, form):
        form.save()
        return redirect("/")


class Profile(LoginRequiredMixin, TemplateView):
    template_name = "profile/profile.html"

    def get_context_data(self, **kwargs):
        context = super(Profile, self).get_context_data(**kwargs)
        if kwargs:
            id_ = kwargs['pk']
            custom_user = CustomUser.objects.get(id=id_)
            context['period'] = custom_user.get_statistic_display()
            context['custom_user'] = custom_user
            stat_choice = custom_user.statistic
        else:
            id_ = self.request.user.id
            context['custom_user'] = self.request.user
            context['period'] = self.request.user.get_statistic_display()
            stat_choice = self.request.user.statistic
        context['rewards'] = UserReward.objects.filter(user_id=id_).select_related("user_id", "reward_id")
        if stat_choice == 3:
            context['products'] = UserProduct.objects.filter(user_id=id_).order_by('-created_at').select_related(
                "user_id", "prod_id")
            context['summary'] = UserProduct.objects.filter(user_id=id_).aggregate(summary=Sum('us_pr_calories'))[
                'summary']
            return context
        elif stat_choice == 1:
            now = datetime.now() - timedelta(weeks=1)
        elif stat_choice == 2:
            now = datetime.now() - timedelta(days=30)
        else:
            raise ValueError("Выбранный период отсутствует")
        context['products'] = UserProduct.objects.filter(user_id=id_, created_at__gte=now).order_by(
            '-created_at').select_related("user_id", "prod_id")
        context['gender'] = self.request.user.get_gender_display()
        context['summary'] = UserProduct.objects.filter(user_id=id_, created_at__gte=now).aggregate(
            summary=Sum('us_pr_calories'))['summary']
        return context


class Statistics(LoginRequiredMixin, TemplateView):
    template_name = "profile/user_stats.html"

    def get_context_data(self, pk, **kwargs):
        context = super(Statistics, self).get_context_data(**kwargs)
        user_id = self.request.user.id
        if pk == 1:
            context['products'] = UserProduct.objects.filter(user_id=user_id).order_by('created_at').select_related(
                "user_id", "prod_id")
            context['summary'] = UserProduct.objects.filter(user_id=user_id).aggregate(summary=Sum('us_pr_calories'))[
                'summary']
            context['period'] = "Статистика за все время"
            return context
        elif pk == 2:
            now = datetime.now() - timedelta(hours=24)
            context['period'] = "Статистика за сутки"
        elif pk == 3:
            now = datetime.now() - timedelta(weeks=1)
            context['period'] = "Статистика за неделю"
        elif pk == 4:
            now = datetime.now() - timedelta(days=30)
            context['period'] = "Статистика за месяц"
        else:
            raise Http404
        context['products'] = UserProduct.objects.filter(user_id=user_id, created_at__gte=now).order_by(
            'created_at').select_related("user_id", "prod_id")
        context['summary'] = UserProduct.objects.filter(user_id=user_id, created_at__gte=now).aggregate(
            summary=Sum('us_pr_calories'))['summary']
        return context


class Settings(CreateView, LoginRequiredMixin):
    template_name = "profile/settings.html"
    model = CustomUser
    form_class = SettingsForm

    def get_form_kwargs(self):
        kwargs = super(Settings, self).get_form_kwargs()
        kwargs['instance'] = CustomUser.objects.get(id=self.request.user.id)
        return kwargs

    def form_valid(self, form):
        form.save()
        return redirect("/accounts/profile/")
