from django_registration.forms import RegistrationForm
from django import forms
from .models import CustomUser, Product


class CustomUserForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = CustomUser


class SettingsForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = (
            'image', 'user_weight', 'user_height', 'statistic', 'age', 'gender'
        )
        labels = {'image': "Аватар:", 'user_weight': "Ваш вес:", 'user_height': "Ваш рост:", 'age': "Ваш возраст:",
                  'statistic': "Какую статистику отображать?", 'gender': "Пол:"}

    def clean_user_height(self):
        if self.cleaned_data['user_height'] > 275:
            raise forms.ValidationError("Не слишком ли высоко? \n Введите пожалуйста свой реальный рост")
        if self.cleaned_data['user_height'] <= 0:
            raise forms.ValidationError("Низковато. \n Введите пожалуйста свой реальный рост")
        return self.cleaned_data['user_height']

    def clean_user_weight(self):
        if self.cleaned_data['user_weight'] > 415:
            raise forms.ValidationError("Введите пожалуйста реальный вес")
        if self.cleaned_data['user_weight'] <= 0:
            raise forms.ValidationError("Ветром не сдувает?) \n Введите пожалуйста свой реальный вес")
        return self.cleaned_data['user_weight']

    def clean_age(self):
        if self.cleaned_data['age'] > 120:
            raise forms.ValidationError("Введите пожалуйста реальный возраст")
        if self.cleaned_data['age'] <= 0:
            raise forms.ValidationError("Введите пожалуйста свой реальный возраст")
        return self.cleaned_data['age']

    def clean_body_mass_index(self):
        return self.cleaned_data['body_mass_index']


class ProductOfferForm(forms.ModelForm):
    is_deleted = forms.BooleanField(initial=True, label="Предложить")

    class Meta:
        model = Product
        fields = (
            'category', 'name', 'calories', 'proteins', 'fats', 'carbohydrates', 'image', 'is_deleted'
        )
        labels = {'category': "Категория:",
                  'name': "Название:",
                  'calories': "Калории:",
                  'proteins': "Белки:",
                  'fats': "Жиры:",
                  'carbohydrates': "Углеводы:",
                  'image': "Картинка продукта:", }

    def clean(self):
        if self.cleaned_data['calories'] <= 0 or self.cleaned_data['proteins'] <= 0 or self.cleaned_data['fats'] <= 0 \
                or self.cleaned_data['carbohydrates'] <= 0:
            raise forms.ValidationError("Значение не может быть отрицательным")
        return self.cleaned_data
