from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import pre_save
from django.dispatch import receiver


class CreatedUpdated(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Category(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField("Изображения", upload_to="categories/", default='categories/anonymous.jpg')

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    calories = models.FloatField(max_length=10)
    proteins = models.FloatField(max_length=10)
    fats = models.FloatField(max_length=10)
    carbohydrates = models.FloatField(max_length=10)
    image = models.ImageField("Изображения", upload_to="products/", default='products/anonymous.jpg')
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Reward(models.Model):
    THIN_FOR_THE_WEEK = 1
    THIN_FOR_THE_MONTH = 2
    THIN_FOR_THE_YEAR = 3
    FAT_FOR_THE_WEEK = 4
    FAT_FOR_THE_MONTH = 5
    FAT_FOR_THE_YEAR = 6

    REWARDS_CHOICES = (
        (THIN_FOR_THE_WEEK, 'Наилегчайший недели'),
        (THIN_FOR_THE_MONTH, 'Наилегчайший месяца'),
        (THIN_FOR_THE_YEAR, 'Наилегчайший года'),
        (FAT_FOR_THE_WEEK, 'Тяжеловес недели'),
        (FAT_FOR_THE_MONTH, 'Тяжеловес месяца'),
        (FAT_FOR_THE_YEAR, 'Тяжеловес года'),
    )
    title = models.PositiveIntegerField(choices=REWARDS_CHOICES, null=True)


class CustomUser(AbstractUser):
    STAT_FOR_THE_WEEK = 1
    STAT_FOR_THE_MONTH = 2
    STAT_FOR_ALL_TIME = 3

    NONE = 1
    MALE = 2
    FEMALE = 3

    STATISTICS_CHOICES = (
        (STAT_FOR_THE_WEEK, 'За неделю'),
        (STAT_FOR_THE_MONTH, 'За месяц'),
        (STAT_FOR_ALL_TIME, 'Вся')
    )
    GENDER_CHOICES = (
        (NONE, 'Не определено'),
        (MALE, 'Мужчина'),
        (FEMALE, 'Женщина')
    )
    image = models.ImageField("Изображения", upload_to="photo/", default='photo/anonymous.jpg')
    user_weight = models.FloatField(max_length=5, null=True)
    user_height = models.FloatField(max_length=5, null=True)
    user_calories = models.FloatField(max_length=255, default=0)
    statistic = models.PositiveSmallIntegerField(choices=STATISTICS_CHOICES, default=3)
    body_mass_index = models.FloatField(max_length=5, null=True)
    age = models.PositiveSmallIntegerField(default=0)
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, default=1)


class UserProduct(CreatedUpdated):
    prod_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    user_id = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    weight = models.FloatField(max_length=10)
    us_pr_calories = models.FloatField(max_length=255, default=0)


class UserReward(CreatedUpdated):
    reward_id = models.ForeignKey(Reward, on_delete=models.CASCADE)
    user_id = models.ForeignKey(CustomUser, on_delete=models.CASCADE)


@receiver(pre_save, sender=CustomUser)
def user_pre_save_action(sender, instance, **kwargs):
    if instance.user_weight and instance.user_height:
        instance.body_mass_index = instance.user_weight / ((instance.user_height / 100) * (instance.user_height / 100))
