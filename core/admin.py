from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe

from core.models import Category, Product, CustomUser, UserReward


class CustomUserAdmin(UserAdmin):
    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("category", "name", "calories", "get_image")
    list_filter = ('is_deleted', 'category')

    def get_image(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" width="50" height="60">')

    get_image.short_description = "Image"


@admin.register(UserReward)
class UserRewardAdmin(admin.ModelAdmin):

    def get_reward_title(self, obj):
        return obj.reward_id.get_title_display()

    get_reward_title.short_description = 'Reward'
    list_display = ('get_reward_title', 'user_id', 'created_at')
    list_filter = ('reward_id__title',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "get_image")

    def get_image(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" width="50" height="60">')

    get_image.short_description = "Image"


admin.site.register(CustomUser, CustomUserAdmin)
